FROM golang:1.12-alpine as build-env

RUN apk add --update --no-cache ca-certificates git

WORKDIR /
RUN mkdir app/
WORKDIR app/

# COPY go.mod and go.sum files to the workspace
COPY go.mod go.sum* ./

# Get dependancies - will also be cached if we won't change mod/sum
RUN go mod download

# COPY the source code as the last step
COPY . .

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/app

FROM alpine
COPY --from=build-env /go/bin/app /go/bin/app
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
CMD ["./go/bin/app"]
