# Twrss - Twitter to RSS service #

HTTP API to generate a rss xml file from a twitter account's feed.

## Method
```
/rss?user=username
```

username is the username (without 'at' character) of the Twitter account you are interested.

## Enviorment variables

Two enviorment variables should be defined: TWITTER_CONSUMER_KEY and TWITTER_CONSUMER_SECRET.
The first with the Twitter API consumer key and the second with the consumer secret. The access
of the app to Twitter API depend on these variables.

## Other links
- [RSS specification](https://validator.w3.org/feed/docs/rss2.html)
