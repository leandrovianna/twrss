package app

import (
	"errors"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
	"twrss/rss"
	"twrss/twitter"
)

type Twrss struct {
	port    int
	twitter twitter.Twitter
}

func NewTwrss(port int) Twrss {
	consumerKey := os.Getenv("TWITTER_CONSUMER_KEY")
	consumerSecret := os.Getenv("TWITTER_CONSUMER_SECRET")

	if consumerKey == "" {
		err := errors.New("Twitter API Consumer key is not defined on enviorment variables.")
		panic(err)
	}

	if consumerSecret == "" {
		err := errors.New("Twitter API Consumer secret is not defined on enviorment variables.")
		panic(err)
	}

	twrss := Twrss{port: port,
		twitter: twitter.NewTwitter(consumerKey, consumerSecret)}
	return twrss
}

func (t *Twrss) Start() error {
	http.HandleFunc("/rss", t.convertTwitterToRss)

	log.Printf("Twrss server listening on localhost:%d\n", t.port)
	err := http.ListenAndServe(":"+strconv.Itoa(t.port), nil)
	return err
}

func (t *Twrss) convertTwitterToRss(rw http.ResponseWriter, r *http.Request) {
	log.Printf("Incoming request from %v\n", r.RemoteAddr)
	if r.Method != "GET" {
		// Only GET Method is allowed
		log.Printf("Method %s not allowed\n", r.Method)
		rw.WriteHeader(405)
		return
	}

	username := r.FormValue("user")

	if len(username) == 0 {
		// a username should be sent
		log.Printf("[From: %v] Request without username\n", r.RemoteAddr)
		rw.WriteHeader(400)
		return
	}

	tweets, err := t.twitter.GetTweets(username)

	if err != nil {
		log.Printf("Internal error: %v\n", err)
		rw.WriteHeader(500)
		return
	}

	log.Printf("[From: %v] Username requested: %s\n", r.RemoteAddr, username)

	rssFile := rss.NewRSS(username, "Twitter feed of "+username, "https://twitter.com/"+username)

	for _, tweet := range tweets {
		item := rss.RSSItem{}
		item.Title = rss.NewCdata(tweet.Text)
		item.Desc = rss.NewCdata(tweet.Text)
		item.PubDate = tweet.CreatedAt.Format(time.RFC822)

		if len(tweet.Entities.Urls) > 0 {
			tweetUrl := tweet.Entities.Urls[0]
			item.Link = tweetUrl.ExpandedUrl
		}

		if len(tweet.Entities.Medias) > 0 {
			tweetMedia := tweet.Entities.Medias[0]
			mediaUrl := tweetMedia.MediaUrl
			if mediaUrl == "" {
				mediaUrl = tweetMedia.Url
			}
			mimeType := "image/jpg"
			item.Enclosure = rss.RSSEnclosure{Url: mediaUrl, Length: 0, MimeType: mimeType}
		}

		rssFile.AddItem(item)
	}

	output, err := rssFile.Build()
	if err != nil {
		rw.WriteHeader(500)
		log.Printf("Internal error: %v\n", err)
		return
	}

	if _, err := rw.Write(output); err != nil {
		log.Printf("Error on writing output: %v\n", err)
	}
}
