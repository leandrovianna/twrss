package main

import (
	"fmt"
	"os"
	"strconv"

	"twrss/app"
)

func getPort() int {
	port, err := strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		return 8080
	}

	return port
}

func main() {
	port := getPort()

	twrss := app.NewTwrss(port)
	err := twrss.Start()
	if err != nil {
		fmt.Println(err)
	}
}
