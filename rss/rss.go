package rss

import (
	"encoding/xml"
)

type RSS struct {
	XMLName xml.Name  `xml:"rss"`
	Title   string    `xml:"channel>title"`
	Desc    string    `xml:"channel>description"`
	Link    string    `xml:"channel>link"`
	Items   []RSSItem `xml:"channel>item"`

	Version string `xml:"version,attr"`
}

func NewRSS(title, desc, link string) RSS {
	return RSS{Version: "2.0", Title: title, Desc: desc, Link: link, Items: make([]RSSItem, 0)}
}

type RSSItem struct {
	Title     cdata        `xml:"title"`
	Desc      cdata        `xml:"description"`
	Link      string       `xml:"link,omitempty"`
	PubDate   string       `xml:"pubDate,omitempty"`
	Enclosure RSSEnclosure `xml:"enclosure,omitempty"`
}

type RSSEnclosure struct {
	Url      string `xml:"url,attr"`
	Length   int    `xml:"length,attr"`
	MimeType string `xml:"type,attr"`
}

type cdata struct {
	Value string `xml:",cdata"`
}

func NewCdata(content string) cdata {
	return cdata{Value: content}
}

func (r *RSS) AddItem(item RSSItem) {
	r.Items = append(r.Items, item)
}

func (r RSS) Build() ([]byte, error) {
	innerRss, err := xml.Marshal(r)
	if err != nil {
		return nil, err
	}

	return append([]byte(xml.Header), innerRss...), nil
}
