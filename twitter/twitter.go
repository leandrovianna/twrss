package twitter

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type Twitter struct {
	consumerKey, consumerSecret string
	accessToken                 string
	client                      http.Client
	tweetsCount                 int
}

type Tweet struct {
	Id        int64
	CreatedAt TwitterTime `json:"created_at"`
	Text      string
	Entities  TwitterEntities
}

type TwitterTime struct {
	time.Time
}

func (t *TwitterTime) UnmarshalJSON(bytes []byte) error {
	var s string
	if err := json.Unmarshal(bytes, &s); err != nil {
		return err
	}

	// twitter api returns time on ruby date format
	if tnew, err := time.Parse(time.RubyDate, s); err != nil {
		return err
	} else {
		// if ok, assign the time unmarshalled
		*t = TwitterTime{tnew}
	}

	return nil
}

type TwitterEntities struct {
	Urls   []TwitterUrl
	Medias []TwitterMedia `json:"media"`
}

type TwitterUrl struct {
	Url         string
	ExpandedUrl string `json:"expanded_url"`
}

type TwitterMedia struct {
	Url      string                  `json:"expanded_url"`
	MediaUrl string                  `json:"media_url,media_url_https,url"`
	Sizes    map[string]TwitterDimen `json:"sizes"`
}

type TwitterDimen struct {
	Height int    `json:"h"`
	Resize string `json:"resize"`
	Width  int    `json:"w"`
}

func NewTwitter(consumerKey, consumerSecret string) Twitter {
	return Twitter{consumerKey: consumerKey, consumerSecret: consumerSecret, client: http.Client{}, tweetsCount: 30}
}

func (t *Twitter) GetTweets(username string) ([]Tweet, error) {
	// get the access token from twitter api
	if err := t.authorization(); err != nil {
		return nil, err
	}

	const twitter_api_user_timeline = "https://api.twitter.com/1.1/statuses/user_timeline.json"

	url := twitter_api_user_timeline + "?" +
		"screen_name=" + username + "&" +
		"count=" + strconv.Itoa(t.tweetsCount) + "&" +
		"trim_user=1"

	request, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return nil, err
	}

	request.Header.Add("Authorization", "Bearer "+t.accessToken)

	response, err := t.client.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	var tweets []Tweet

	decoder := json.NewDecoder(response.Body)
	if err := decoder.Decode(&tweets); err != nil {
		return nil, err
	}

	return tweets, nil
}

func (t *Twitter) authorization() error {
	if t.accessToken != "" {
		// already authorized
		return nil
	}

	const twitter_api_request_token_url = "https://api.twitter.com/oauth2/token"
	bearerToken := base64.StdEncoding.EncodeToString([]byte(
		url.QueryEscape(t.consumerKey) + ":" + url.QueryEscape(t.consumerSecret)))

	body := strings.NewReader("grant_type=client_credentials")
	request, err := http.NewRequest("POST", twitter_api_request_token_url, body)

	if err != nil {
		return err
	}

	request.Header.Add("Authorization", "Basic "+bearerToken)
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")

	response, err := t.client.Do(request)

	if err != nil {
		return err
	}

	defer response.Body.Close()

	jsonDecoder := json.NewDecoder(response.Body)

	payload := make(map[string]interface{})
	if err := jsonDecoder.Decode(&payload); err != nil {
		return err
	}

	if tokenType, ok := payload["token_type"]; !ok || tokenType != "bearer" {
		return errors.New("Token type returned is not bearer")
	}

	if accessToken, ok := payload["access_token"].(string); !ok {
		return errors.New("Access token not found on payload")
	} else {
		t.accessToken = accessToken
	}

	return nil
}
